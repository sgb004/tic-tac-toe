#include<iostream>
#include<time.h>
#include<unistd.h>

int size;
int titleSpace;
int places[9][9];
char moves[9][9];

void printDashboard(){
	system("clear");

	std::cout << "Tic-Tac-Toe\n";

	std::cout << "\nLugares";
	for(int i=0; i<titleSpace; i++){
		std::cout << " ";
	}

	std::cout << "Movimientos\n";
	for(int i=0; i<size; i++){
		if(size == 3){
        	for(int j=0; j<size; j++){
           		std::cout << "[";
                if(places[i][j] == 0){                                          
					std::cout << "-";
				}else{
					std::cout << places[i][j];
				}
				std::cout << "]";                                
			}                        
		}else{                         
			for(int j=0; j<size; j++){
				std::cout << "[";
				if(places[i][j] == 0){
					std::cout << "--";
				}else{      
					if(places[i][j] < 10){     
						std::cout << " ";      
					}      
					std::cout << places[i][j];
				}
				std::cout << "]";
			}
		}
		std::cout << " ";
		for(int j=0; j<size; j++){
			std::cout << "[" << moves[i][j] << "]";
		}
		std::cout << "\n"; 
	}
}

void start(){
	int player, place, row, column, movesCount = 0, j = 1;
	char piece;
	bool win = false, equal;

	srand(time(NULL));

	std::cout << "Tic-Tac-Toe\n";

	do{
		std::cout << "\nSelecciona el tamaño de la rejilla,\ndebe ser un numero entero mayor entre 3 y 9: ";
		std::cin >> size;
	}while(size < 3 || size > 9);

	int size1 = size - 1;
	int size2 = size * size;
	int size4 = size2 + 1;
	int posSize;

	for(int i=0; i<size; i++){
		for(int k=0; k<size; k++){
			places[i][k] = j;
			moves[i][k] = ' ';
			j++;
		}
	}
	
	do{
		player = rand() % 100 + 1;		
		if(player < 50){
			player = 2;
		}else{
			player = 1;
		}
		std::cout << "\nJugador " << player << " selecciona tipo de ficha, (x) (o): ";
		std::cin >> piece;
		
		if(piece != 'x' && piece != 'o'){
			std::cout << "La ficha " << piece << " seleccionada no esta disponible.\n";
		}
	}while(piece != 'x' &&  piece != 'o');

	titleSpace = ((size - 3) + 1) * 3;

	if(size > 3){
		titleSpace += size;
	}
	
	do{
		printDashboard();

		do{
			std::cout << "\nJugador " << player << " selecciona un lugar: ";
			std::cin >> place;

			row = (place - 1) / size;
			column = place - 1 - ( size * row );

			if(place < 1 || place > size2 || places[row][column] == 0){
				std::cout << "El lugar seleccionado no esta disponible, por favor intenta de nuevo.\n";
			}
		}while(place < 1 || place > size2 || places[row][column] == 0);

		places[row][column] = 0;
		moves[row][column] = piece;

		equal = true;
		for(int i=0; i<size1; i++){
			if(moves[i][i] != moves[i+1][i+1]
			|| moves[i][i] == ' ' 
			|| moves[i+1][i+1] == ' '){
				equal = false;
			}
		}

		if(equal == true){
			win = true;
			movesCount = size4;
		}

		if(!win){
			equal = true;
			j = size1;
			for(int i=0; i<size1; i++){
				if(moves[i][j] != moves[i+1][j-1]
				|| moves[i][j] == ' ' 
				|| moves[i+1][j-1] == ' '){
					equal = false;
				}
				j--;
			}

			if(equal == true){
				win = true;
				movesCount = size4;
			}
		}

		if(!win){
			posSize = size * 2;
			
			for(int i=0; i<size; i++){
				bool hasx = false;
				bool haso = false;
				bool hase = false;

				for(int j=0; j<size; j++){
					if(moves[i][j] == 'x'){
						hasx = true;
					}else if(moves[i][j] == 'o'){
						haso = true;
					}else{
						hase = true;
					}
				}

				if((hasx && !haso && !hase) || (!hasx && haso && !hase)){
					win = true;
					movesCount = size4;
					break;
				}else if(hasx && haso){
					posSize--;
				}

				hasx = false;
				haso = false;
				hase = false;

				for(int j=0; j<size; j++){
					if(moves[j][i] == 'x'){
						hasx = true;
					}else if(moves[j][i] == 'o'){
						haso = true;
					}else{
						hase = true;
					}
				}

				if((hasx && !haso && !hase) || (!hasx && haso && !hase)){
					win = true;
					movesCount = size4;
					break;
				}else if(hasx && haso){
					posSize--;
				}
			}

			if(posSize <= 0){
				movesCount = size4;
			}
		}

		if(!win){
			if(piece == 'x'){
            	piece = 'o';
            }else{
	           piece = 'x';
    	    }                     

			if(player == 1){      
				player = 2;
			}else{
				player = 1;
			}
		}

		movesCount++;

	}while(movesCount < size4);

	printDashboard();

	std::cout << "\n";
	if(win){
		std::cout << "xoxoxoxoxoxoxoxoxoxoxoxoxoxoxoxoxoxo\n";
		std::cout << "o                                  x\n";
		std::cout << "x   Felicidades jugador " << player << " ganaste  o\n";
		std::cout << "o                                  x\n";
		std::cout << "xoxoxoxoxoxoxoxoxoxoxoxoxoxoxoxoxoxo\n";
	}else{
		std::cout << "xoxoxoxoxoxoxoxoxoxoxoxoxoxoxoxoxoxo\n";
		std::cout << "o                                  x\n";
		std::cout << "x  Sin posibilidad de que algun@   o\n";
		std::cout << "o          jugador@ gane           x\n";
		std::cout << "x                                  o\n";
		std::cout << "oxoxoxoxoxoxoxoxoxoxoxoxoxoxoxoxoxox\n";
	}
}

int main(){
	char c;

	system("clear");
	
	std::cout << "xoxoxoxoxoxoxoxoxoxoxoxoxoxoxoxoxoxo\n";
	std::cout << "o                                  x\n";
	std::cout << "x            Bienvenid@            o\n";
	std::cout << "o                                  x\n";
	std::cout << "xoxoxoxoxoxoxoxoxoxoxoxoxoxoxoxoxoxo\n";

	sleep(2);
	system("clear");

	std::cout << "xoxoxoxoxoxoxoxoxoxoxoxoxoxoxoxoxoxo\n";
	std::cout << "o                                  x\n";
	std::cout << "x                 a                o\n";
	std::cout << "o                                  x\n";
	std::cout << "xoxoxoxoxoxoxoxoxoxoxoxoxoxoxoxoxoxo\n";

	sleep(2);
	system("clear");

	std::cout << "xoxoxoxoxoxoxoxoxoxoxoxoxoxoxoxoxoxo\n";
	std::cout << "o                                  x\n";
	std::cout << "x           Tic-Tac-Toe            o\n";
	std::cout << "o                                  x\n";
	std::cout << "xoxoxoxoxoxoxoxoxoxoxoxoxoxoxoxoxoxo\n";

	sleep(2);
	
	do{
		system("clear");

		start();
		std::cout << "\n¿Volver a jugar? [y/n] ";
		std::cin >> c;
	}while(c != 'n');

	std::cout << "\n";
	
	return 0;
}
